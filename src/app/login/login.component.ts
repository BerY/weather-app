import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { 
    FormGroup, 
    FormControl,
    Validators, 
    FormBuilder } from '@angular/forms';

import { AuthService } from '../auth.service';

interface User {
  name: String;
  password: String;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(private _authService: AuthService, private _router: Router,
              fb: FormBuilder) {
                      this.form = fb.group({
          "name": new FormControl(""),
          "password": new FormControl("")
      });
 }

  onSubmit({ value, valid }: { value: User, valid: boolean }) {
    console.log(value, valid);
   
    this._authService.login(value.name, value.password)
      .subscribe((result) => {
        if (result) {
          this._router.navigate(['home']);
        }
      });
  }

  ngOnInit() {
  }
}