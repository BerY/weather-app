import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Headers, Response } from '@angular/http';

import { ChartData } from './models/chartdata.model';
import { Profile } from './models/profile.model';

import 'rxjs/add/operator/map';

@Injectable()
export class ProfileService {

  private API_URL = "http://localhost:8080/api/profile";
  
  constructor(private _http: Http) { }

  updatePassword(body:Profile) : Observable<any> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let authToken = localStorage.getItem('auth_token');
    headers.append('Authorization', `${authToken}`);

    let bodyString :string = JSON.stringify(body);

    return this._http.post(this.API_URL,  bodyString, { headers })
  }
}
