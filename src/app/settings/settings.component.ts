import { Component, OnInit } from '@angular/core';
import { FormGroup,
         FormControl,
         Validators, 
         FormBuilder }  from '@angular/forms';

import { Settings } from '../models/settings.model';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  form: FormGroup;
  cleanForm: FormGroup;

  constructor(private _settingsService:SettingsService, private _fb: FormBuilder) { }

  onSubmit({ value, valid }: { value: Settings, valid: boolean }) {
    this._settingsService.updateSettings(value)
       .subscribe((res) => {});
  }

  clean() {
    this._settingsService.clean()
       .subscribe((res) => {});
  }

  ngOnInit() {
    this.form = this._fb.group({
      "email": new FormControl(""),
      "send": new FormControl(false),
      "min": new FormControl(0),
      "max": new FormControl(0)
    });

    this.cleanForm = this._fb.group({});

    this._settingsService.getSettings()
      .subscribe((res) => {
        this.form.controls['email'].setValue(res.email);
        this.form.controls['send'].setValue(res.send);
        this.form.controls['min'].setValue(res.min);
        this.form.controls['max'].setValue(res.max);
      });
  }
}
