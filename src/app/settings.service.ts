import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Headers, Response } from '@angular/http';

import { Settings } from './models/settings.model';

import 'rxjs/add/operator/map';

@Injectable()
export class SettingsService {

  private API_URL:string = "http://localhost:8080/api/settings";

  constructor(private _http: Http) { }

  updateSettings(body:Settings) : Observable<any> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let authToken = localStorage.getItem('auth_token');
    headers.append('Authorization', `${authToken}`);
    let bodyString = JSON.stringify(body);

    return this._http
      .post(this.API_URL, bodyString, { headers })
      .map((res => res.json()));
  }

  getSettings() : Observable<Settings> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let authToken = localStorage.getItem('auth_token');
    headers.append('Authorization', `${authToken}`);

    return this._http
      .get(this.API_URL, { headers })
      .map(res => res.json());
  }

  clean() {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let authToken = localStorage.getItem('auth_token');
    headers.append('Authorization', `${authToken}`);
    let bodyString = JSON.stringify({});

    return this._http
      .post(this.API_URL + '/clean', bodyString, { headers })
      .map((res => res.json()));
  }
}
