export class Profile {
    constructor(
        public oldpass: string,
        public newpass: string,
        public newpassretyped: string
    ){}
}