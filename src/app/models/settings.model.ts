export class Settings {
    constructor(
        public email: string,
        public send: boolean,
        public min: number,
        public max: number
    ) {}
}