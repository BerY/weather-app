import { Injectable } from '@angular/core';
import { CanActivate,
         RouterStateSnapshot,
         ActivatedRouteSnapshot } from '@angular/router';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private _router: Router, private _authService: AuthService) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {
 
    if (!this._authService.isLoggedIn())
       this._router.navigate([ 'login' ]);
       
    return this._authService.isLoggedIn();
  }
}