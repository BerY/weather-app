import { Component, OnInit } from '@angular/core';
import { FormGroup,
         FormControl,
         Validators,
         FormBuilder } from '@angular/forms';
    
import { ProfileService } from '../profile.service';
import { Profile } from '../models/profile.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {
  
  form: FormGroup;

  constructor(private _profileService:ProfileService, private _fb: FormBuilder) { }

  onSubmit({ value, valid }: { value: Profile, valid: boolean }) {
    this._profileService.updatePassword(value)
      .subscribe((res) => {});
  }

  ngOnInit() {
    this.form = this._fb.group({
      "oldpass": new FormControl("", Validators.required ),
      "newpass": new FormControl("", Validators.required),
      "newpassretyped": new FormControl("", Validators.required)
    });
  }
}