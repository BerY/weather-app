import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { appRoutes } from './app.routing';
import { AppComponent } from './app.component';

import { LoginComponent } from './login/login.component';

import { HomeComponent } from './home/home.component';
import { ChartComponent } from './chart/chart.component';
import { SettingsComponent } from './settings/settings.component';
import { ProfileComponent } from './profile/profile.component';

import { ChartsModule } from 'ng2-charts/ng2-charts';

import { AuthService } from "./auth.service"
import { AuthGuardService } from './auth-guard.service'
import { ProfileService } from './profile.service';
import { SettingsService } from './settings.service';
import { StraingaugeComponent } from './straingauge/straingauge.component';
import { StatsComponent } from './stats/stats.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ChartComponent,
    SettingsComponent,
    ProfileComponent,
    StraingaugeComponent,
    StatsComponent,
    NavbarComponent
  ],
  
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    ChartsModule
  ],
  providers: [AuthService, AuthGuardService, ProfileService, SettingsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
