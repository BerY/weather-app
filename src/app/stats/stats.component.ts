import { Component, OnInit } from '@angular/core';
import { FormBuilder,
         FormControl,
         FormGroup, 
         Validators } from '@angular/forms';

import { RaspberryService } from '../raspberry.service';

enum DateRange {
  H4 = 4,
  H8 = 8,
  H12 = 12,
  H24 = 24
};

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css'],
  providers: [RaspberryService]
})
export class StatsComponent implements OnInit {

  form: FormGroup;

  public dataranges = DateRange;
  public keys; 

  public _stats;

  constructor(private _raspberry:RaspberryService, private _fb: FormBuilder) {
      this.keys = Object.keys(this.dataranges).filter(Number);
      this.getStats(DateRange.H24);
  }

  onSubmit({ value, valid }) { 
      this.getStats(value.datarange);
  }

  getStats(datarange) {
     this._raspberry.getStats(datarange)
      .subscribe( (stats) => {
        this._stats = stats[0];
      });
  }

  ngOnInit() {
     this.form = this._fb.group({
          "datarange": new FormControl(DateRange.H4, Validators.required ),
      });
  }
}
