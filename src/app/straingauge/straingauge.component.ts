import { Component, OnInit } from '@angular/core';
import { RaspberryService } from '../raspberry.service';


@Component({
  selector: 'app-straingauge',
  templateUrl: './straingauge.component.html',
  styleUrls: ['./straingauge.component.css'],
  providers: [RaspberryService]
})
export class StraingaugeComponent implements OnInit {
 
  public temperature;

  constructor(private _raspberry:RaspberryService) { }

  ngOnInit() {
    this._raspberry.getCurrentTemparature()
      .subscribe( (res) => {
        this.temperature = res;
      });
  }
}
