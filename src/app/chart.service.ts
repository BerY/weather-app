import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { URLSearchParams, RequestOptions, Http, Headers, Response } from '@angular/http';

import { ChartData } from './models/chartdata.model';

import 'rxjs/add/operator/map';

@Injectable()
export class ChartService {

  constructor(private _http: Http) { }
  
  getDefaultChart(scope:string) : Observable<any> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let authToken = localStorage.getItem('auth_token');
    headers.append('Authorization', `${authToken}`);
    let options = new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})});
    
    options.headers = headers;
    
    return this._http
      .get(
        'http://localhost:8080/api/chart?range='+scope,
        options
      )
      .map((r: Response) => r.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  }
}
