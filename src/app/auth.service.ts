import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Rx';

@Injectable()
export class AuthService {

  private loggedIn:boolean = false; 

  constructor(private http: Http, private _router: Router) {
    this.loggedIn = !!localStorage.getItem('auth_token');
  }

  login(name:String, password:String) : Observable<any>
  {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http
      .post(
        'http://localhost:8080/api/authenticate', 
        JSON.stringify({ name, password }), 
        { headers }
      )
      .map(res => res.json())
      .map((res) => {
        if (res.success) {
          localStorage.setItem('auth_token', res.auth_token);
          this.loggedIn = true;
        }
        return res.success;
      });
  }
  
  logout() : void {
    localStorage.removeItem('auth_token');
    this.loggedIn = false;

    this._router.navigate([ 'login' ]);
       
  }

  isLoggedIn() : boolean {
    return this.loggedIn;
  }
}
