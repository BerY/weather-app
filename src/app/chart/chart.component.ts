import { Component, OnInit } from '@angular/core';
import { ChartService } from '../chart.service';
import { FormGroup,
         FormControl,
         Validators,
         FormBuilder } from '@angular/forms';

import { ChartData } from '../models/chartdata.model';
import 'rxjs/Rx';

enum ChartRange {
  D1 = 1,
  D2 = 2,
  D7 = 7,
  D14 = 14
};

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
  providers: [ChartService]
})
export class ChartComponent {

  form: FormGroup;

  public dataranges = ChartRange;
  public keys; 
  public cdata: ChartData;

  public lineChartData:Array<any>;
  public lineChartZeros:Array<any>;
  public lineChartLabels:Array<any>;
  public lineChartType:string;
  public pieChartType:string;
  public pieChartLabels:string[];
  public pieChartData:number[];

  private lineChartLabelsDay = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24];
  private lineChartLabels2Day = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,
                                1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24];

  private lineChartLabelsWeek = ["P","W", "S", "CZ", "Pt", "SB", "N"];
  private lineChartLabels2Week = ["P","W", "S", "CZ", "Pt", "SB", "N", 
                                  "P","W", "S", "CZ", "Pt", "SB", "N"];
  
  toType = function(obj) {
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
  }

  constructor(private _chartSvc:ChartService, private _fb: FormBuilder) { 
    
    this.keys = Object.keys(this.dataranges).filter(Number);

    this.lineChartType = 'line'; 
    this.pieChartType = 'pie';
    this.pieChartLabels = ['Download Sales']; 
    this.pieChartData = [300, 500, 100]; 

    this.getLabels('2');
    this.getData('2');
  }

  onSubmit({ value, valid }) {
     this.getLabels(value.datarange);
     this.getData(value.datarange);
  }

  ngOnInit() {

     this.form = this._fb.group({
          "datarange": new FormControl(ChartRange.D1, Validators.required ),
      });
  }

  private getData(scope:string) : void
  {
    this._chartSvc.getDefaultChart(scope)
      .subscribe( (res) => {
        this.lineChartData = res;

      });
  }
3
  private getLabels(scope:string) :void {

      let d = new Date();
      let n = d.getDay();
      let k = 0;
      let labels:Array<any>;

      switch (scope)
      {
        case '1':
            k = 24 - d.getHours();
            labels = this.lineChartLabelsDay; 
            break;

        case '2':
            k = 24 - d.getHours();
            labels = this.lineChartLabels2Day;
            break;

        case '7':
            k = 7 - d.getDay();
            labels = this.lineChartLabelsWeek;
            break;
        case '14':
            k = 7  - d.getDay();
            labels = this.lineChartLabels2Week;
            break;
        default:
            k = d.getDay();
            labels = this.lineChartLabelsWeek;
            break;     
      }

      this.lineChartLabels = this.rotate(labels, k);
      this.lineChartZeros = Array.apply(null, new Array(this.lineChartLabels.length)).map(Number.prototype.valueOf,0);
  }

  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }

  private reverseArr(arr, left, right) {

      while (left < right) {
          var temp = arr[left];
          arr[left] = arr[right];
          arr[right] = temp;
          left++;
          right--;
      }
      return arr;
  }

  private rotate(arr, k) {

    var l = arr.length;
    arr = this.reverseArr(arr, 0, l - 1);
    arr = this.reverseArr(arr, 0, k - 1);
    arr = this.reverseArr(arr, k, l - 1);
    return arr;
  }
  
}